# Blogspace Vistula

### Blog app to create and read articles


## Setup

Clone repo
```
git clone https://gitlab.com/shams0910/blogspace-vistula.git
cd blogspace-vistula
```

Run server
```bash
python manage.py runserver
```