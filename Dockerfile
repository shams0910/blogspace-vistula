FROM python:3.9.4-alpine

WORKDIR /usr/src/socratus

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev \
    && apk add jpeg-dev zlib-dev libjpeg \
    && apk add libmagic openssl-dev libffi-dev cargo \
    && apk add gettext \
    && apk add libxml2-dev libxslt-dev

COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .