from django.shortcuts import render, redirect
from django.views import View
from core.models import Blog, Tag
from core.forms import BlogForm


class BlogList(View):
    def get(self, request):
        context = {}
        blogs = Blog.objects.all()
        context['blogs'] = blogs
        return render(request, 'blog_list.html', context)


class AddBlog(View):
    def get(self, request):
        return render(request, 'add_blog.html')

    def post(self, request):
        context = {}
        form = BlogForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        else:
            context['errors'] = form.errors
            return render(request, 'add_blog.html', context)

        return redirect('blog_list')




