from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=30)


class Blog(models.Model):
    title = models.TextField()
    content = models.TextField()
    picture = models.FileField()

