from django.urls import path
from . import views


urlpatterns = [
    path('', views.BlogList.as_view(), name='blog_list'),
    path('add_blog', views.AddBlog.as_view(), name='add_blog')
]